#!/bin/bash
pause(){
 read -n1 -rsp $'Press any key to continue...\n'
}

# Create config files
tee -a /etc/radvd.conf <<EOF
interface vpeer-router { 
        AdvSendAdvert on;
        MinRtrAdvInterval 3; 
        MaxRtrAdvInterval 10;
        prefix 2001::/64 { 
                AdvOnLink on; 
                AdvAutonomous on; 
                AdvRouterAddr on; 
        };
};
interface veth3 { 
        AdvSendAdvert on;
        MinRtrAdvInterval 3; 
        MaxRtrAdvInterval 10;
        prefix 2002::/64 { 
                AdvOnLink on; 
                AdvAutonomous on; 
                AdvRouterAddr on; 
        };
};
EOF

pause

echo $'\n'Create resources
ip netns add h1
ip netns add h2
ip netns add h3
ip netns add h4
ip netns add r1
ip link add name veth1 type veth peer name vpeer1
ip link add name veth2 type veth peer name vpeer2
ip link add name veth3 type veth peer name vpeer3
ip link add name veth4 type veth peer name vpeer4
ip link add name veth-router type veth peer name vpeer-router
brctl addbr sw1

pause

echo $'\n'Set peer link up
ip link set veth1 up
ip link set veth2 up
ip link set veth3 up
ip link set veth4 up
ip link set veth-router up
ip link set sw1 up

pause

echo $'\n'Assign interfaces to namespaces 
ip link set dev vpeer1 netns h1
ip link set dev vpeer2 netns h2
ip link set dev vpeer3 netns h3
ip link set dev vpeer4 netns h4
ip link set dev vpeer-router netns r1
ip link set dev veth1 netns r1

pause

echo $'\n'Connect veth to bridge
brctl addif sw1 veth2
brctl addif sw1 veth3
brctl addif sw1 veth4
brctl addif sw1 veth-router

pause

echo $'\n'Configure router as router
ip netns exec r1 sysctl -w net.ipv4.ip_forward=1

pause


echo $'\n'Configure IP addresses
ip netns exec h1 ip addr add 192.168.1.10/24 dev vpeer1
ip netns exec r1 ip addr add 192.168.1.11/24 dev veth1
ip netns exec r1 ip addr add 192.168.2.12/24 dev vpeer-router
ip netns exec h4 ip addr add 192.168.2.1/24 dev vpeer4

pause

echo $'\n'Configure DHCP Server
gnome-terminal -- /bin/sh -c 'ip netns exec h4 dnsmasq --dhcp-range=192.168.2.2,192.168.2.254,255.255.255.0 --dhcp-option=3,192.168.2.12 --dhcp-host=192.168.2.12 --interface=vpeer4 --no-daemon'

pause

echo $'\n'Set Up interfaces
ip netns exec h1 ip link set lo up
ip netns exec h2 ip link set lo up
ip netns exec h3 ip link set lo up
ip netns exec h4 ip link set lo up
ip netns exec r1 ip link set lo up

ip netns exec h1 ip link set vpeer1 up
ip netns exec h2 ip link set vpeer2 up
ip netns exec h3 ip link set vpeer3 up
ip netns exec h4 ip link set vpeer4 up
ip netns exec r1 ip link set veth1 up
ip netns exec r1 ip link set vpeer-router up

pause

echo $'\n'Add default gateway
ip netns exec h1 ip route add default via 192.168.1.11 dev vpeer1
ip netns exec h4 ip route add default via 192.168.2.12 dev vpeer4

pause

echo $'\n'Configure DHCP Clients
gnome-terminal -- /bin/sh -c 'ip netns exec h2 dhclient -d'
gnome-terminal -- /bin/sh -c 'ip netns exec h3 dhclient -d'

pause

echo $'\n'Init router advertisement daemon
ip netns exec r1 radvd -n



